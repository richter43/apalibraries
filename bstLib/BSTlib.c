#include "BSTlib.h"




bstNode *createBstNode(bstNode *root,void *data)
{
	bstNode *newNode =malloc(sizeof(bstNode));
	
	if(newNode==NULL)
	{
		fprintf(stderr, "%s\n", "prob with malloc");
		return NULL;
	}
	
	newNode->left=NULL;
	newNode->right=NULL;
	newNode->data=data;
	newNode->parrent=root;
	return newNode;
}


bstNode *addBstNode(bstNode *root,void *data, int (*compare)(void *,void *))
{
	if(root==NULL)
	{
		return createBstNode(root,data);
	}
	int result = compare(root->data,data);
	if(result>0)
	{
		if(root->right!=NULL)
		{
			return addBstNode(root->right,data,compare);	
		}
		else
		{
			root->right=createBstNode(root,data);
			if(root->right==NULL)
			{
				fprintf(stderr, "%s\n", "problem with node creation");
				return NULL;
			}
		}
	}
	else if(result<0)
	{
		if(root->left!=NULL)
		{
			return addBstNode(root->left,data,compare);
		}
		else
		{
			root->left=createBstNode(root,data);
			if(root->left==NULL)
			{
				fprintf(stderr, "%s\n", "problem with node creation");
				return NULL;	
			}
		}
	}
	else 
	{
		return root;// it's allready in the fucking thing >:0	
	}
	return NULL;
}

void printBstO(bstNode *root,void (*print)(void *))
{
	if(root->left!=NULL)
	{
		printBstO(root->left,print);
	}
	print(root->data);
	if(root->right!=NULL)
	{
		printBstO(root->right,print);
	}
}

void printBstPR(bstNode *root,void (*print)(void *))
{

	print(root->data);
	if(root->left!=NULL)
	{
		printBstPR(root->left,print);
	}
	
	if(root->right!=NULL)
	{
		printBstPR(root->right,print);
	}
}

void printBstPO(bstNode *root,void (*print)(void *))
{

	
	if(root->left!=NULL)
	{
		printBstPO(root->left,print);
	}
	if(root->right!=NULL)
	{
		printBstPO(root->right,print);
	}
	print(root->data);
}

bstNode *getMinBstNode(bstNode *root)
{
	while(root->left!=NULL)
	{
		root=root->left;
	}
	return root;
}

bstNode *getMaxBstNode(bstNode *root)
{
	while(root->right!=NULL)
	{
		root=root->right;
	}
	return root;
}

bstNode *getBstNode(bstNode *root,void *data,int (*compare)(void *,void *))
{
	int result = compare(root->data,data);
	if(result==0)
	{
		return root;
	}
	else if(result>0)
	{
		if(root->right!=NULL)
		{
			return getBstNode(root->right,data,compare);
		}
		else
		{
			return NULL;
		}
	}
	else if(result<0)
	{
		if(root->left!=NULL)
		{
			return getBstNode(root->left,data,compare);
		}
		else
		{
			return NULL;
		}
	}
	else 
	{
		return NULL;
	}
}


bstNode *deleteBst(bstNode *root,void *data,int (*compare)(void *,void *),void (*deleteData)(void *))
{
	
	if(root==NULL)
	{
		return root;
	}
	bstNode *curs = getBstNode(root,data,compare);

	if(curs==NULL)
	{
		return root;
	}
	

	bstNode *parrent = curs->parrent;
	if(curs->left == NULL && curs->right ==NULL)
	{
		if(parrent)
		{
			if(parrent->left==curs)
			{
				parrent->left=NULL;
			}
			else 
			{
				parrent->right=NULL;
			}
		}
		if(deleteData!=NULL)
		{
			deleteData(curs->data);
		}
		if(curs==root)
		{
			free(curs);
			return NULL;
		}
		free(curs);
		curs=NULL;
		return root;
	}
	else if(curs->right!=NULL&&curs->left!=NULL)
	{

		bstNode *sub = getMinBstNode(curs->right);
		void *tmp = sub->data;
		curs->data=tmp;
		deleteBst(curs->right,tmp,compare,NULL);// this shouldn't change the root if it does idk bad stuff
		
	}
	else
	{
		bstNode *child;
		if(curs->left!=NULL)
		{
			child = curs->left;
		}
		else
		{
			child = curs->right;
		}
		child->parrent=parrent;
		if(parrent!=NULL)
		{

			if(curs==parrent->left)
			{
				parrent->left=child;
			}
			else
			{
				parrent->right=child;
			}

			if(deleteData!=NULL)
			{
				deleteData(curs->data);
			}
			free(curs);
			curs =NULL;
			return root;// we haven't changed root 
		}
		else
		{	
			if(deleteData!=NULL)
			{
				deleteData(curs->data);
			}

			free(curs);
			curs=NULL;

			return child;//we are changing root
		}

		
	}
	return root;

}