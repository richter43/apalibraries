#include <stdio.h>
#include <stdlib.h>

typedef struct bstNode
{
	struct bstNode 	*parrent,*left,*right;
	void *data;

}bstNode;
/*	
	create bst Nodes creates a new bstNode and setts the parrent to root
	this is usefull if you want to create a new tree;
	The data has to be already on the heap and souldn't be free 

*/
bstNode *createBstNode(bstNode *root,void *data);
/*
	addBstNode adds an node to an already exsisting tree using the compares function to check if it has to go left 
	or right during the search fase. the data argument is the "key" that you are comparing the node's data to.
	the second argument to the compare function is the void *data passed to createBst Node 
	i.e

	int result = compare(root->data,data);
	therfore an simple compare function for ints is 
	int compareInt(void *rootData,void *data)
	{
		return(*(int *)data-*(int *)rootData);
	}// this will return a postive int if the "data" is bigger than the key of the node 0 if it's the same and a negative value if it is less
	if a node with that key already exists it doesn't create a new one instead it returns the node. 
	The function if it creates a node it returns the node created.
*/
bstNode *addBstNode(bstNode *root,void *data, int (*compare)(void *,void *));
/*
	Various types of priting functions requiring a print function for that datatype
*/
void printBstO(bstNode *root,void (*print)(void *));
void printBstPR(bstNode *root,void (*print)(void *));
void printBstPO(bstNode *root,void (*print)(void *));
/* 
	Get's a pointer to the biggest and smallest element of the tree 
*/
bstNode *getMinBstNode(bstNode *root);
bstNode *getMaxBstNode(bstNode *root);
/* 
	Returns a certian node searching using the same method as createBstNode
*/
bstNode *getBstNode(bstNode *root,void *data,int (*compare)(void *,void *));
/*
	The name says it all searches using the same method as create bst node it updates the tree returing the new or old root 
	Very important that you do root = deleteBst(bleh)
	if not you are in for some funky ass bugs 
*/
bstNode *deleteBst(bstNode *root,void *data,int (*compare)(void *,void *),void (*deleteData)(void *));
